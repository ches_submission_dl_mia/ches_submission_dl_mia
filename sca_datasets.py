from ches_submission_dl_mia.trs.trace import Trace


class ScaDataSets:

    def __init__(self, param, trace_directory_path):
        self.trace_directory_path = trace_directory_path
        self.param = param

    def load_test_set(self, random_order=False):
        test_trs = Trace(self.param["trs_file_test"], self.param["first_sample"], self.param["number_of_samples"],
                         self.param["n_test"], self.param["data_length"])

        test_trs.set_trace_directory_path(self.trace_directory_path)
        [test_samples, x_test_data] = test_trs.open_trace(random_order=random_order)
        x_test = test_samples.reshape((test_samples.shape[0], test_samples.shape[1], 1))

        return x_test, x_test_data, test_samples

    def load_two_tests(self, random_order=False):
        test_trs = Trace(self.param["trs_file_test"], self.param["first_sample"], self.param["number_of_samples"],
                         self.param["n_test"], self.param["data_length"])

        test_trs.set_trace_directory_path(self.trace_directory_path)
        [test_samples, x_test_data] = test_trs.open_trace(random_order=random_order)
        x_test = test_samples.reshape((test_samples.shape[0], test_samples.shape[1], 1))

        nt = int(self.param["n_test"] / 2)

        x_test_1 = x_test[0:nt]
        x_test_2 = x_test[nt:self.param["n_test"]]
        x_test_data_1 = x_test_data[0:nt]
        x_test_data_2 = x_test_data[nt:self.param["n_test"]]

        return x_test_1, x_test_2, x_test_data_1, x_test_data_2

    def load_training_and_validation_sets(self, random_order=False):
        training_trs = Trace(self.param["trs_file_training"], self.param["first_sample"],
                             self.param["number_of_samples"], self.param["n_train"] + self.param["n_validation"],
                             self.param["data_length"])
        training_trs.set_trace_directory_path(self.trace_directory_path)

        [trace_samples, trace_data] = training_trs.open_trace(random_order=random_order)

        training_dataset_reshaped = trace_samples.reshape((trace_samples.shape[0], trace_samples.shape[1], 1))

        x_train = training_dataset_reshaped[0:self.param["n_train"]]
        x_val = training_dataset_reshaped[self.param["n_train"]:self.param["n_train"] + self.param["n_validation"]]
        y_train_data = trace_data[0:self.param["n_train"]]
        y_val_data = trace_data[self.param["n_train"]:self.param["n_train"] + self.param["n_validation"]]
        train_samples = trace_samples[0:self.param["n_train"]]
        val_samples = trace_samples[self.param["n_train"]:self.param["n_train"] + self.param["n_validation"]]

        return x_train, x_val, y_train_data, y_val_data, train_samples, val_samples
