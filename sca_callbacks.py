from keras.callbacks import Callback
import keras.backend as backend
from ches_submission_dl_mia.IDNNs.idnns import get_information
import numpy as np
import tensorflow as tf


class TrainingCallback(Callback):
    def __init__(self, x_data, y_data):
        self.current_epoch = 0
        self.x = x_data
        self.y = y_data
        self.accuracy = []
        self.recall = []
        self.loss = []

    def on_epoch_end(self, epoch, logs={}):
        with tf.device('/cpu:0'):
            loss, acc, recall = self.model.evaluate(self.x, self.y, verbose=0)

        self.accuracy.append(acc)
        self.recall.append(recall)
        self.loss.append(loss)

    def get_accuracy(self):
        return self.accuracy

    def get_recall(self):
        return self.recall

    def get_loss(self):
        return self.loss


class ValidationCallback(Callback):
    def __init__(self, x_data, y_data, save_model=False):
        self.current_epoch = 0
        self.x = x_data
        self.y = y_data
        self.accuracy = []
        self.recall = []
        self.loss = []
        self.save_model = save_model

    def on_epoch_end(self, epoch, logs={}):
        with tf.device('/cpu:0'):
            loss, acc, recall = self.model.evaluate(self.x, self.y, verbose=0)
            # loss, acc = self.model.evaluate(self.x, self.y, verbose=0)

        self.accuracy.append(acc)
        self.recall.append(recall)
        self.loss.append(loss)

        if self.save_model:
            if epoch > 0:
                max_acc = np.max(self.accuracy[0:epoch])
                save_model = False
                for epoch_index in range(len(self.accuracy)):
                    if self.accuracy[epoch] > max_acc:
                        save_model = True
                if save_model:
                    self.model.save_weights("best_model_accuracy.h5")
                    print("model saved acc =" + str(self.accuracy[epoch]) + ") at epoch " + str(epoch))

                min_loss = np.min(self.loss[0:epoch])
                save_model = False
                for epoch_index in range(len(self.loss)):
                    if self.loss[epoch] < min_loss:
                        save_model = True
                if save_model:
                    self.model.save_weights("best_model_loss.h5")
                    print("model saved loss =" + str(self.loss[epoch]) + ") at epoch " + str(epoch))

                max_recall = np.max(self.recall[0:epoch])
                save_model = False
                for epoch_index in range(len(self.recall)):
                    if self.recall[epoch] > max_recall:
                        save_model = True
                if save_model:
                    self.model.save_weights("best_model_recall.h5")
                    print("model saved recall =" + str(self.recall[epoch]) + ") at epoch " + str(epoch))
            else:
                self.model.save_weights("best_model_accuracy.h5")
                self.model.save_weights("best_model_loss.h5")
                self.model.save_weights("best_model_recall.h5")

    def get_accuracy(self):
        return self.accuracy

    def get_recall(self):
        return self.recall

    def get_loss(self):
        return self.loss


class InputGradients(Callback):
    def __init__(self, x_data):
        self.current_epoch = 0
        self.x = x_data

    def on_train_end(self, logs=None):
        session = backend.get_session()
        y_val_d_evaluated = session.run(tf.gradients(self.model.output, self.model.input),
                                        feed_dict={self.model.input: self.x})
        grad = y_val_d_evaluated[0]

        # grads = np.zeros((len(grad), len(grad[0])))
        grads = np.zeros(len(grad[0]))
        for i in range(len(grad)):
            for j in range(len(grad[0])):
                # grads[i][j] = grad[i][j]
                grads[j] += grad[i][j]

        import matplotlib.pyplot as plt
        # for i in range(len(grad)):
        #     plt.plot(grads[i])
        plt.plot(grads)
        plt.show()


class KeyRankingCallbackEpochEnd(Callback):
    def __init__(self, x_val, labels_key_hypothesis, param):
        self.current_epoch = 0
        self.labels_key_hypothesis = labels_key_hypothesis
        self.x_val = x_val
        self.key_ranking = np.zeros((param["epochs"
                                     ], len(x_val)))
        self.correct_key_byte = param["good_key"]
        self.number_of_key_hypothesis = param["number_of_key_hypothesis"]

    def on_epoch_end(self, epoch, logs={}):

        with tf.device('/cpu:0'):
            output_probabilities = self.model.predict(self.x_val)
        key_probabilities = np.zeros(self.number_of_key_hypothesis)

        for index in range(len(self.x_val)):

            probabilities_kg = output_probabilities[index][
                np.asarray([int(l[index]) for l in self.labels_key_hypothesis[:]])]
            key_probabilities += np.log(probabilities_kg)

            key_probabilities_sorted = np.argsort(key_probabilities)[::-1]
            for kg in range(self.number_of_key_hypothesis):
                if key_probabilities_sorted[kg] == self.correct_key_byte:
                    self.key_ranking[epoch][index] = kg + 1

    def get_key_ranking(self):
        return self.key_ranking


class KeyRankingCallbackTrainingEnd(Callback):
    def __init__(self, x_val, labels_key_hypothesis, param):
        self.current_epoch = 0
        self.labels_key_hypothesis = labels_key_hypothesis
        self.x_val = x_val
        self.key_ranking = np.zeros(len(x_val))
        self.correct_key_byte = param["good_key"]
        self.number_of_key_hypothesis = param["number_of_key_hypothesis"]

    def on_train_end(self, logs=None):

        with tf.device('/cpu:0'):
            output_probabilities = self.model.predict(self.x_val)
        key_probabilities = np.zeros(self.number_of_key_hypothesis)

        for index in range(len(self.x_val)):

            probabilities_kg = output_probabilities[index][
                np.asarray([int(l[index]) for l in self.labels_key_hypothesis[:]])]
            key_probabilities += np.log(probabilities_kg)

            key_probabilities_sorted = np.argsort(key_probabilities)[::-1]
            for kg in range(self.number_of_key_hypothesis):
                if key_probabilities_sorted[kg] == self.correct_key_byte:
                    self.key_ranking[index] = kg + 1

    def get_key_ranking(self):
        return self.key_ranking


class KeyProbabilitiesCallbackEpochEnd(Callback):
    def __init__(self, x_val, labels_key_hypothesis, param):
        self.current_epoch = 0
        self.labels_key_hypothesis = labels_key_hypothesis
        self.x_val = x_val
        self.key_probabilities = np.zeros((param["epochs"], param["number_of_key_hypothesis"], len(x_val)))
        self.correct_key_byte = param["good_key"]
        self.number_of_key_hypothesis = param["number_of_key_hypothesis"]

    def on_epoch_end(self, epoch, logs={}):

        with tf.device('/cpu:0'):
            output_probabilities = self.model.predict(self.x_val)

        for index in range(len(self.x_val)):

            probabilities_kg = output_probabilities[index][
                np.asarray(
                    [
                        int(leakage[index]) for leakage in self.labels_key_hypothesis[:]
                    ]
                )
            ]

            for kg in range(self.number_of_key_hypothesis):
                self.key_probabilities[epoch][kg][index] = np.log(probabilities_kg[kg])

    def get_key_probabilities(self):
        return self.key_probabilities


class CalculateMutualInformationAllLayers(Callback):
    def __init__(self, input_data, labels, number_of_bins, ixt=False):
        self.input_data = input_data
        self.labels = labels
        self.idx = 0
        self.mia_all_layers = {}
        self.mia_all_layers_epochs = []
        self.number_of_bins = number_of_bins
        self.ixt = ixt
        self.input_reshaped = self.input_data.reshape((self.input_data.shape[0], self.input_data.shape[1], 1))

    def on_epoch_end(self, epoch, logs=None):
        from keras.models import Model
        activations = []

        layer_name = None
        outputs = [layer.output for layer in self.model.layers if layer.name == layer_name or layer_name is None][1:]

        for output in outputs:
            intermediate_model = Model(inputs=self.model.input, outputs=output)
            activations.append(intermediate_model.predict(self.input_reshaped))

        self.mia_all_layers = {}
        for layer_index in range(len(activations)):
            print("Computing MI for layer " + str(layer_index))
            if self.ixt:
                self.mia_all_layers['l_' + str(layer_index + 1)] = bin_calc_information(self.input_data,
                                                                                        activations[layer_index],
                                                                                        self.number_of_bins)
            else:
                self.mia_all_layers['l_' + str(layer_index + 1)] = bin_calc_information(self.labels,
                                                                                        activations[layer_index],
                                                                                        self.number_of_bins)
        self.mia_all_layers_epochs.append(self.mia_all_layers)

        # mia = get_information(activations, self.input_data, self.labels, self.number_of_bins, 50, calc_parallel=False)
        # self.mia_all_layers.append(mia)

    def get_mia(self):
        return self.mia_all_layers_epochs


class CalculateMutualInformationOutputLayer(Callback):
    def __init__(self, input_data, labels, number_of_bins):
        self.input_data = input_data
        self.labels = labels
        self.mia_output_layer = []
        self.mia_bins = []
        self.number_of_bins = number_of_bins

    def on_epoch_end(self, epoch, logs=None):
        print("Getting Activations...")
        activations = []
        activations.append(
            self.model.predict(self.input_data.reshape((self.input_data.shape[0], self.input_data.shape[1], 1))))

        mia = get_information(activations, self.input_data, self.labels, self.number_of_bins, 50, calc_parallel=False)
        self.mia_output_layer.append(mia)

    def get_mia(self):
        return self.mia_output_layer


class CalculateITY(Callback):
    def __init__(self, input_data, labels, number_of_bins, save_model=False, max_ity=0):
        self.input_data = input_data
        self.input_reshaped = self.input_data.reshape((self.input_data.shape[0], self.input_data.shape[1], 1))
        self.labels = labels
        self.ity = []
        self.mia_bins = []
        self.number_of_bins = number_of_bins
        self.save_model = save_model
        self.max_ity = max_ity

    def on_epoch_end(self, epoch, logs=None):
        print("Getting Activations...")
        activations = []
        # activations.append(
        #     self.model.predict(self.input_data.reshape((self.input_data.shape[0], self.input_data.shape[1], 1))))
        activations.append(self.model.predict(self.input_reshaped))

        for activation in activations:
            self.ity.append(bin_calc_information(self.labels, activation, self.number_of_bins))

        if self.save_model:
            if epoch > 0:
                max_ity_all_epochs = np.max(self.ity[0:epoch])
                save_model = False
                for epoch_index in range(len(self.ity)):
                    if self.ity[epoch] > max_ity_all_epochs:
                        save_model = True
                if save_model:
                    if self.ity[epoch] > self.max_ity:
                        self.max_ity = self.ity[epoch]
                        # self.model.save_weights("best_model_ity.h5")
                        self.model.save("best_model_ity.h5")
                        print("model saved (I(T,Y) =" + str(self.ity[epoch]) + ") at epoch " + str(epoch))
            else:
                self.model.save("best_model_ity.h5")

    def get_ity(self):
        return self.ity

    def get_max_ity(self):
        return self.max_ity


class CalculateITYBins(Callback):
    def __init__(self, input_data, labels, bins_min, bins_max, number_of_epochs):
        self.input_data = input_data
        self.input_reshaped = self.input_data.reshape((self.input_data.shape[0], self.input_data.shape[1], 1))
        self.labels = labels
        self.ity = np.zeros((bins_max - bins_min, number_of_epochs))
        self.mia_bins = []
        self.bins_min = bins_min
        self.bins_max = bins_max

    def on_epoch_end(self, epoch, logs=None):
        print("Getting Activations...")
        activations = []
        # activations.append(
        #     self.model.predict(self.input_data.reshape((self.input_data.shape[0], self.input_data.shape[1], 1))))
        activations.append(self.model.predict(self.input_reshaped))

        for activation in activations:
            for bins in range(self.bins_min, self.bins_max):
                self.ity[bins - self.bins_min][epoch] = bin_calc_information(self.labels, activation, bins)

    def get_ity(self):
        return self.ity


class CalculateIXY(Callback):
    def __init__(self, input_data, labels, number_of_bins):
        self.input_data = input_data
        self.labels = labels
        self.ity = []
        self.mia_bins = []
        self.number_of_bins = number_of_bins

    def on_epoch_end(self, epoch, logs=None):
        print("Getting Activations...")
        activations = []
        activations.append(
            self.model.predict(self.input_data.reshape((self.input_data.shape[0], self.input_data.shape[1], 1))))

        for activation in activations:
            self.ity.append(bin_calc_information(self.input_data, activation, self.number_of_bins))

    def get_ixy(self):
        return self.ity


def get_unique_probabilities(x):
    unique_ids = np.ascontiguousarray(x).view(np.dtype((np.void, x.dtype.itemsize * x.shape[1])))
    _, unique_inverse, unique_counts = np.unique(unique_ids, return_index=False, return_inverse=True,
                                                 return_counts=True)
    return np.asarray(unique_counts / float(sum(unique_counts))), unique_inverse


def bin_calc_information(input_data, activations, num_of_bins):
    p_xs, unique_inverse_x = get_unique_probabilities(input_data)

    bins = np.linspace(-1, 1, num_of_bins, dtype='float32')
    digitized = bins[np.digitize(np.squeeze(activations.reshape(1, -1)), bins) - 1].reshape(len(activations), -1)
    p_ts, _ = get_unique_probabilities(digitized)

    entropy_activations = -np.sum(p_ts * np.log(p_ts))
    entropy_activations_given_input = 0.
    for x in np.arange(len(p_xs)):
        p_t_given_x, _ = get_unique_probabilities(digitized[unique_inverse_x == x, :])
        entropy_activations_given_input += - p_xs[x] * np.sum(p_t_given_x * np.log(p_t_given_x))
    return entropy_activations - entropy_activations_given_input


class GetWeights(Callback):
    # Keras callback which collects values of weights and biases at each epoch
    def __init__(self):
        super(GetWeights, self).__init__()
        self.weight_dict_epochs = []
        self.weight_dict = {}

    def on_epoch_end(self, epoch, logs=None):
        self.weight_dict = {}

        # loop over each layer and get weights and biases
        for layer_i in range(2, len(self.model.layers)):
            w = self.model.layers[layer_i].get_weights()[0]
            b = self.model.layers[layer_i].get_weights()[1]
            print('Layer %s has weights of shape %s and biases of shape %s' % (
                layer_i, np.shape(w), np.shape(b)))

            self.weight_dict['w_' + str(layer_i + 1)] = w
            self.weight_dict['b_' + str(layer_i + 1)] = b

        self.weight_dict_epochs.append(self.weight_dict)

    def get_weights(self):
        return self.weight_dict_epochs
