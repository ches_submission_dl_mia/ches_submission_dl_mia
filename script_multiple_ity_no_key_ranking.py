import numpy as np
from keras import backend
from keras.utils import to_categorical
from ches_submission_dl_mia.deep_learning_models.deeplearning import DeepLearningModel
from ches_submission_dl_mia.crypto.aes import AES
from ches_submission_dl_mia.trs_parameters import TrsParameters
from ches_submission_dl_mia.sca_callbacks import ValidationCallback, CalculateITY
from ches_submission_dl_mia.sca_datasets import ScaDataSets
from ches_submission_dl_mia.sca_plots import Plots

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#

trace_directory_path = 'D:/traces/'
trs_parameters = TrsParameters()
param = trs_parameters.get_trace_set("ascad_random_key")

# ---------------------------------------------------------------------------------------------------------------------#
#  Define leakage model for AES:
#  "round_state_input": 1 or 10
#  "round_state_output": 1 or 10
#  "state_output": SubBytesOut,
#  (state input is only necessary for HD)
#  (state output is considered for HW, ID and bit)
#  "leakage_model": HW or bit
#  "bit": 0(LSB) - 7(MSB) (index of the bit in a byte)
#  "byte": 0-15 (index of the byte in the AES state)
#  "operation": encryption, decryption
#  "direction": input, output
# ---------------------------------------------------------------------------------------------------------------------#

aes_leakage_model = {
    "round_state_input": 1,
    "round_state_output": 1,
    "state_input": "",
    "state_output": "SubBytesOut",
    "leakage_model": "HW",
    "bit": 0,
    "byte": 2,
    "operation": "encryption",
    "direction": "input"
}

if aes_leakage_model["leakage_model"] == "HW":
    param["classes"] = 9
elif aes_leakage_model["leakage_model"] == "ID":
    param["classes"] = 256
else:
    param["classes"] = 2

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#

data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, trace_data_train, trace_data_validation, train_dataset, validation_dataset = data_sets.load_training_and_validation_sets()
x_test_fixed_key, x_validation_fixed_key, trace_data_test_fixed_key, trace_data_validation_fixed_key = data_sets.load_two_tests(
    random_order=True)

# ---------------------------------------------------------------------------------------------------------------------#
#  Create categorical labels from selected leakage model and cipher
#  A categorical label is a vector with all zeros, except at the index of the class/label
#  Example: if label is 6 from a total amount of class of 9, categorical label is [0, 0, 0, 0, 0, 0, 1, 0, 0]
# ---------------------------------------------------------------------------------------------------------------------#

crypto = AES()
train_labels = crypto.create_labels(trace_data_train, aes_leakage_model, param)
validation_labels = crypto.create_labels(trace_data_validation, aes_leakage_model, param)
validation_labels_fixed_key = crypto.create_labels(trace_data_validation_fixed_key, aes_leakage_model, param)
test_labels_fixed_key = crypto.create_labels(trace_data_test_fixed_key, aes_leakage_model, param)

y_train = to_categorical(train_labels, num_classes=param["classes"])
y_validation = to_categorical(validation_labels, num_classes=param["classes"])
y_validation_fixed_key = to_categorical(validation_labels_fixed_key, num_classes=param["classes"])
y_test_fixed_key = to_categorical(test_labels_fixed_key, num_classes=param["classes"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Create labels for each key byte hypothesis for test set in order to calculate key ranking
# ---------------------------------------------------------------------------------------------------------------------#

number_of_traces_test = int(param["n_test"] / 2)
labels_key_hypothesis_test_fixed_key = np.zeros((param["number_of_key_hypothesis"], number_of_traces_test))
labels_key_hypothesis_validation_fixed_key = np.zeros((param["number_of_key_hypothesis"], number_of_traces_test))

for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
    key_h = bytearray.fromhex(param["key"])
    key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
    crypto.set_key(key_h)
    labels_key_hypothesis_test_fixed_key[key_byte_hypothesis][:] = crypto.create_labels(
        trace_data_test_fixed_key, aes_leakage_model, param, key_hypothesis=True
    )
    labels_key_hypothesis_validation_fixed_key[key_byte_hypothesis][:] = crypto.create_labels(
        trace_data_validation_fixed_key, aes_leakage_model, param, key_hypothesis=True
    )

# ---------------------------------------------------------------------------------------------------------------------#
#  Loop for the number of training runs (for success rate calculation)
# ---------------------------------------------------------------------------------------------------------------------#

number_of_runs = 1

key_ranking_ity = np.zeros((number_of_runs, len(x_test_fixed_key)))
key_ranking_acc = np.zeros((number_of_runs, len(x_test_fixed_key)))
key_ranking_loss = np.zeros((number_of_runs, len(x_test_fixed_key)))
key_ranking_recall = np.zeros((number_of_runs, len(x_test_fixed_key)))

ITY_average_validation = np.zeros(param["epochs"])
best_epoch_ITY_all_runs = np.zeros(number_of_runs)

for run in range(number_of_runs):

    print("Run: " + str(run))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare callbacks
    # -----------------------------------------------------------------------------------------------------------------#

    callbacks_validation = ValidationCallback(x_validation, y_validation, save_model=True)
    callback_mia_validation_ity = CalculateITY(validation_dataset, y_validation, 100, save_model=True)

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare neural network model and run training by calling .fit()
    # -----------------------------------------------------------------------------------------------------------------#

    model_obj = DeepLearningModel()
    model = model_obj.basic_mlp_hw_aes_20gs(param["classes"], param["number_of_samples"])
    model.fit(x=x_train,
              y=y_train,
              batch_size=400,
              verbose=1,
              epochs=param["epochs"],
              shuffle=True,
              validation_data=(x_validation, y_validation),
              callbacks=[callbacks_validation, callback_mia_validation_ity])

    # ---------------------------------------------------------------------------------------------------------------- #
    # Process Mutual Information
    # ---------------------------------------------------------------------------------------------------------------- #

    ity_validation = callback_mia_validation_ity.get_ity()
    ITY_average_validation += ity_validation

    best_epoch_ITY = np.argmax(ity_validation)
    best_epoch_ITY_all_runs[run] = best_epoch_ITY

    # -----------------------------------------------------------------------------------------------------------------#
    # Process key ranks at epochs with best validation metrics (I(T,Y), accuracy, loss, recall)
    # -----------------------------------------------------------------------------------------------------------------#

    model.load_weights("best_model_ity.h5")
    output_probabilities_ity = model.predict(x_test_fixed_key)
    model.load_weights("best_model_accuracy.h5")
    output_probabilities_acc = model.predict(x_test_fixed_key)
    model.load_weights("best_model_loss.h5")
    output_probabilities_loss = model.predict(x_test_fixed_key)
    model.load_weights("best_model_recall.h5")
    output_probabilities_recall = model.predict(x_test_fixed_key)

    key_probabilities_ity = np.zeros(param["number_of_key_hypothesis"])
    key_probabilities_acc = np.zeros(param["number_of_key_hypothesis"])
    key_probabilities_loss = np.zeros(param["number_of_key_hypothesis"])
    key_probabilities_recall = np.zeros(param["number_of_key_hypothesis"])

    for index in range(len(x_test_fixed_key)):

        probabilities_kg_ity = output_probabilities_ity[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis_test_fixed_key[:]])]
        key_probabilities_ity += np.log(probabilities_kg_ity)

        probabilities_kg_acc = output_probabilities_acc[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis_test_fixed_key[:]])]
        key_probabilities_acc += np.log(probabilities_kg_acc)

        probabilities_kg_loss = output_probabilities_loss[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis_test_fixed_key[:]])]
        key_probabilities_loss += np.log(probabilities_kg_loss)

        probabilities_kg_recall = output_probabilities_recall[index][
            np.asarray([int(l[index]) for l in labels_key_hypothesis_test_fixed_key[:]])]
        key_probabilities_recall += np.log(probabilities_kg_recall)

        key_probabilities_sorted_ity = np.argsort(key_probabilities_ity)[::-1]
        key_probabilities_sorted_acc = np.argsort(key_probabilities_acc)[::-1]
        key_probabilities_sorted_loss = np.argsort(key_probabilities_loss)[::-1]
        key_probabilities_sorted_recall = np.argsort(key_probabilities_recall)[::-1]

        for kg in range(param["number_of_key_hypothesis"]):

            if key_probabilities_sorted_ity[kg] == param["good_key"]:
                key_ranking_ity[run][index] = kg + 1

            if key_probabilities_sorted_acc[kg] == param["good_key"]:
                key_ranking_acc[run][index] = kg + 1

            if key_probabilities_sorted_loss[kg] == param["good_key"]:
                key_ranking_loss[run][index] = kg + 1

            if key_probabilities_sorted_recall[kg] == param["good_key"]:
                key_ranking_recall[run][index] = kg + 1

    backend.clear_session()

# -------------------------------------------------------------------------------------------------------------------- #
#  Compute Guessing Entropy = Averaged Key Ranking over multiple trainings
# ---------------------------------------------------------------------------------------------------------------------#

guessing_entropy_acc = np.zeros(number_of_traces_test)
guessing_entropy_recall = np.zeros(number_of_traces_test)
guessing_entropy_loss = np.zeros(number_of_traces_test)
guessing_entropy_ity = np.zeros(number_of_traces_test)

for run in range(number_of_runs):
    guessing_entropy_acc += key_ranking_acc[run]
    guessing_entropy_recall += key_ranking_recall[run]
    guessing_entropy_loss += key_ranking_loss[run]
    guessing_entropy_ity += key_ranking_ity[run]

guessing_entropy_acc /= number_of_runs
guessing_entropy_recall /= number_of_runs
guessing_entropy_loss /= number_of_runs
guessing_entropy_ity /= number_of_runs

# -------------------------------------------------------------------------------------------------------------------- #
#  Compute Success Rates
# ---------------------------------------------------------------------------------------------------------------------#

success_rate_acc = np.zeros(number_of_traces_test)
success_rate_recall = np.zeros(number_of_traces_test)
success_rate_loss = np.zeros(number_of_traces_test)
success_rate_ITY = np.zeros(number_of_traces_test)

for index in range(number_of_traces_test):
    for run in range(number_of_runs):
        if key_ranking_acc[run][index] == 1:
            success_rate_acc[index] += 1
        if key_ranking_recall[run][index] == 1:
            success_rate_recall[index] += 1
        if key_ranking_loss[run][index] == 1:
            success_rate_loss[index] += 1
        if key_ranking_ity[run][index] == 1:
            success_rate_ITY[index] += 1

success_rate_acc /= number_of_runs
success_rate_recall /= number_of_runs
success_rate_loss /= number_of_runs
success_rate_ITY /= number_of_runs

# -------------------------------------------------------------------------------------------------------------------- #
#  Compute average ITY, average IXY
# ---------------------------------------------------------------------------------------------------------------------#

average_ITY = ITY_average_validation / number_of_runs

# -------------------------------------------------------------------------------------------------------------------- #
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(rows=1, cols=2)
plots.new_plot()

list_of_series = [
    {"data": guessing_entropy_acc, "label": "GE Accuracy", "color": "blue", "legend": True},
    {"data": guessing_entropy_recall, "label": "GE Recall", "color": "orange", "legend": True},
    {"data": guessing_entropy_loss, "label": "GE Loss", "color": "green", "legend": True},
    {"data": guessing_entropy_ity, "label": "GE ITY", "color": "purple", "legend": True}
]
plots.create_line_plot(1, list_of_series, "Number of Traces", "Guessing Entropy", show_legend=True)

list_of_series = [
    {"data": success_rate_acc, "label": "SR Accuracy", "color": "blue", "legend": True},
    {"data": success_rate_recall, "label": "SR Recall", "color": "orange", "legend": True},
    {"data": success_rate_loss, "label": "SR Loss", "color": "green", "legend": True},
    {"data": success_rate_ITY, "label": "SR ITY", "color": "purple", "legend": True}
]
plots.create_line_plot(2, list_of_series, "Number of Traces", "Success Rate", show_legend=True)

plots.show_plot()

backend.clear_session()
