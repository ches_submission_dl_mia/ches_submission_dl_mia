class TrsParameters:

    def __init__(self):
        self.trace_set_list = []

    def get_trace_set(self, trace_set_name):
        trace_list = self.get_trace_set_list()
        return trace_list[trace_set_name]

    def get_trace_set_list(self):

        parameters_ascad_random_key = {
            "key": "00112233445566778899AABBCCDDEEFF",
            "key_offset": 16,
            "input_offset": 0,
            "data_length": 50,
            "first_sample": 0,
            "number_of_samples": 1400,
            "n_train": 99000,
            "n_validation": 1000,
            "n_test": 1000,
            "trs_file_training": "ASCAD_variable_key + TraceNormalization.trs",
            "trs_file_test": "ASCAD_fixed_key + TraceNormalization.trs",
            "classes": 9,
            "good_key": 34,
            "number_of_key_hypothesis": 256,
            "epochs": 100,
            "random_key": True
        }

        parameters_ches_ctf = {
            "key": "2EEE5E799D72591C4F4C10D8287F397A",
            "key_offset": 32,
            "input_offset": 0,
            "data_length": 48,
            "first_sample": 0,
            "number_of_samples": 2200,
            "n_train": 43000,
            "n_validation": 2000,
            "n_test": 5000,  # 5000 available
            "trs_file_training": "ChesCTF_training_45k + TraceNormalization.trs",
            "trs_file_test": "ChesCTF_test_5k + TraceNormalization.trs",
            "classes": 9,
            "good_key": 46,
            "number_of_key_hypothesis": 256,
            "epochs": 50,
            "random_key": True
        }

        parameters_dpa_v4 = {
            "key": "6cecc67f287d083deb8766f0738b36cf",
            "key_offset": 32,
            "input_offset": 0,
            "pt_byte": 0,
            "data_length": 32,
            "first_sample": 0,
            "number_of_samples": 2000,
            "n_train": 34000,
            "n_validation": 2000,
            "n_test": 4000,
            "trs_file_training": "dpa_contest_v4_keyByte0 + Training.trs",
            "trs_file_test": "dpa_contest_v4_keyByte0 + Test.trs",
            "classes": 9,
            "good_key": 108,
            "number_of_key_hypothesis": 256,
            "epochs": 200,
            "random_key": False
        }

        self.trace_set_list = {
            "ascad_random_key": parameters_ascad_random_key,
            "ches_ctf": parameters_ches_ctf,
            "dpa_v4": parameters_dpa_v4
        }

        return self.trace_set_list
