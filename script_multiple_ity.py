import numpy as np
from keras import backend
from keras.utils import to_categorical
from ches_submission_dl_mia.deep_learning_models.deeplearning import DeepLearningModel
from ches_submission_dl_mia.crypto.aes import AES
from ches_submission_dl_mia.trs_parameters import TrsParameters
from ches_submission_dl_mia.sca_callbacks import TrainingCallback, ValidationCallback, KeyRankingCallbackEpochEnd, CalculateITY
from ches_submission_dl_mia.sca_datasets import ScaDataSets
from ches_submission_dl_mia.sca_plots import Plots

# ---------------------------------------------------------------------------------------------------------------------#
#  Set trs files directory path and target params
# ---------------------------------------------------------------------------------------------------------------------#

trace_directory_path = 'D:/traces/'
trs_parameters = TrsParameters()
param = trs_parameters.get_trace_set("ascad_random_key")

# ---------------------------------------------------------------------------------------------------------------------#
#  Define leakage model for AES:
#  "round_state_input": 1 or 10
#  "round_state_output": 1 or 10
#  "state_output": SubBytesOut,
#  (state input is only necessary for HD)
#  (state output is considered for HW, ID and bit)
#  "leakage_model": HW or bit
#  "bit": 0(LSB) - 7(MSB) (index of the bit in a byte)
#  "byte": 0-15 (index of the byte in the AES state)
#  "operation": encryption, decryption
#  "direction": input, output
# ---------------------------------------------------------------------------------------------------------------------#

aes_leakage_model = {
    "round_state_input": 1,
    "round_state_output": 1,
    "state_input": "",
    "state_output": "SubBytesOut",
    "leakage_model": "HW",
    "bit": 7,
    "byte": 2,
    "operation": "encryption",
    "direction": "input"
}

if aes_leakage_model["leakage_model"] == "HW":
    param["classes"] = 9
elif aes_leakage_model["leakage_model"] == "ID":
    param["classes"] = 256
else:
    param["classes"] = 2

# ---------------------------------------------------------------------------------------------------------------------#
#  Create Training, Validation and Test set based on target params
# ---------------------------------------------------------------------------------------------------------------------#

data_sets = ScaDataSets(param, trace_directory_path)
x_train, x_validation, trace_data_train, trace_data_validation, train_dataset, validation_dataset = data_sets.load_training_and_validation_sets()
x_test_fixed_key, x_validation_fixed_key, trace_data_test_fixed_key, trace_data_validation_fixed_key = data_sets.load_two_tests(
    random_order=True)

# ---------------------------------------------------------------------------------------------------------------------#
#  Create categorical labels from selected leakage model and cipher
#  A categorical label is a vector with all zeros, except at the index of the class/label
#  Example: if label is 6 from a total amount of class of 9, categorical label is [0, 0, 0, 0, 0, 0, 1, 0, 0]
# ---------------------------------------------------------------------------------------------------------------------#

crypto = AES()
train_labels = crypto.create_labels(trace_data_train, aes_leakage_model, param)
validation_labels = crypto.create_labels(trace_data_validation, aes_leakage_model, param)
validation_labels_fixed_key = crypto.create_labels(trace_data_validation_fixed_key, aes_leakage_model, param)
test_labels_fixed_key = crypto.create_labels(trace_data_test_fixed_key, aes_leakage_model, param)

y_train = to_categorical(train_labels, num_classes=param["classes"])
y_validation = to_categorical(validation_labels, num_classes=param["classes"])
y_validation_fixed_key = to_categorical(validation_labels_fixed_key, num_classes=param["classes"])
y_test_fixed_key = to_categorical(test_labels_fixed_key, num_classes=param["classes"])

# ---------------------------------------------------------------------------------------------------------------------#
#  Create labels for each key byte hypothesis for test set in order to calculate key ranking
# ---------------------------------------------------------------------------------------------------------------------#

number_of_traces_test = int(param["n_test"] / 2)
labels_key_hypothesis_test_fixed_key = np.zeros((param["number_of_key_hypothesis"], number_of_traces_test))
labels_key_hypothesis_validation_fixed_key = np.zeros((param["number_of_key_hypothesis"], number_of_traces_test))

for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
    key_h = bytearray.fromhex(param["key"])
    key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
    crypto.set_key(key_h)
    labels_key_hypothesis_test_fixed_key[key_byte_hypothesis][:] = crypto.create_labels(
        trace_data_test_fixed_key, aes_leakage_model, param, key_hypothesis=True
    )
    labels_key_hypothesis_validation_fixed_key[key_byte_hypothesis][:] = crypto.create_labels(
        trace_data_validation_fixed_key, aes_leakage_model, param, key_hypothesis=True
    )

# ---------------------------------------------------------------------------------------------------------------------#
#  Loop for the number of training runs (for success rate calculation)
# ---------------------------------------------------------------------------------------------------------------------#

number_of_runs = 1
key_rank_test_all_runs = np.zeros((number_of_runs, param["epochs"], number_of_traces_test))
key_rank_val_all_runs = np.zeros((number_of_runs, param["epochs"], number_of_traces_test))

best_key_rankings_acc = np.zeros((number_of_runs, number_of_traces_test))
best_key_rankings_recall = np.zeros((number_of_runs, number_of_traces_test))
best_key_rankings_loss = np.zeros((number_of_runs, number_of_traces_test))
best_key_rankings_validation_key_rank = np.zeros((number_of_runs, number_of_traces_test))
best_key_rankings_ITY = np.zeros((number_of_runs, number_of_traces_test))

ITY_average_validation = np.zeros(param["epochs"])

best_epoch_ITY_all_runs = np.zeros(number_of_runs)
best_epoch_validation_key_rank_all_runs = np.zeros(number_of_runs)

for run in range(number_of_runs):

    print("Run: " + str(run))

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare callbacks
    # -----------------------------------------------------------------------------------------------------------------#

    callbacks_training = TrainingCallback(x_train[0:param["n_validation"]], y_train[0:param["n_validation"]])
    callbacks_validation = ValidationCallback(x_validation, y_validation)
    callback_key_ranking_epoch_test = KeyRankingCallbackEpochEnd(x_test_fixed_key,
                                                                 labels_key_hypothesis_test_fixed_key,
                                                                 param)
    callback_key_ranking_epoch_validation = KeyRankingCallbackEpochEnd(x_validation_fixed_key,
                                                                       labels_key_hypothesis_validation_fixed_key,
                                                                       param)
    callback_mia_validation_ity = CalculateITY(validation_dataset, y_validation, 100)

    # -----------------------------------------------------------------------------------------------------------------#
    #  Declare neural network model and run training by calling .fit()
    # -----------------------------------------------------------------------------------------------------------------#

    model_obj = DeepLearningModel()
    model = model_obj.basic_mlp_ascad_random_keys(param["classes"], param["number_of_samples"])
    model.fit(x=x_train,
              y=y_train,
              batch_size=400,
              verbose=1,
              epochs=param["epochs"],
              shuffle=True,
              validation_data=(x_validation, y_validation),
              callbacks=[callbacks_training, callbacks_validation, callback_key_ranking_epoch_test,
                         callback_key_ranking_epoch_validation, callback_mia_validation_ity])

    # ---------------------------------------------------------------------------------------------------------------- #
    #  Retrieve metrics (accuracy, recall, loss) and key ranking from callbacks
    # ---------------------------------------------------------------------------------------------------------------- #

    accuracy_training = callbacks_training.get_accuracy()
    accuracy_validation = callbacks_validation.get_accuracy()

    recall_training = callbacks_training.get_recall()
    recall_validation = callbacks_validation.get_recall()

    loss_training = callbacks_training.get_loss()
    loss_validation = callbacks_validation.get_loss()

    key_rank_test_epochs = callback_key_ranking_epoch_test.get_key_ranking()
    key_rank_validation_epochs = callback_key_ranking_epoch_validation.get_key_ranking()

    for epoch in range(param["epochs"]):
        key_rank_test_all_runs[run][epoch] = key_rank_test_epochs[epoch]
        key_rank_val_all_runs[run][epoch] = key_rank_validation_epochs[epoch]

    ity_validation = callback_mia_validation_ity.get_ity()

    # ---------------------------------------------------------------------------------------------------------------- #
    # Compute epoch with best validation metrics (accuracy, recall, loss)
    # ---------------------------------------------------------------------------------------------------------------- #

    best_key_rankings_acc[run] = key_rank_test_epochs[np.argmax(accuracy_validation)]
    best_key_rankings_recall[run] = key_rank_test_epochs[np.argmax(recall_validation)]
    best_key_rankings_loss[run] = key_rank_test_epochs[np.argmin(loss_validation)]

    # ---------------------------------------------------------------------------------------------------------------- #
    # Compute epoch with best validation key ranking
    # ---------------------------------------------------------------------------------------------------------------- #
    key_ranks_number_of_traces = []
    for epoch in range(param["epochs"]):
        if key_rank_validation_epochs[epoch][number_of_traces_test - 1] == 1:
            for index in range(number_of_traces_test - 1, -1, -1):
                if key_rank_validation_epochs[epoch][index] != 1:
                    key_ranks_number_of_traces.append(
                        [key_rank_validation_epochs[epoch][number_of_traces_test - 1], index + 1, epoch])
                    break
        else:
            key_ranks_number_of_traces.append(
                [key_rank_validation_epochs[epoch][number_of_traces_test - 1], number_of_traces_test, epoch])

    sorted_models = sorted(key_ranks_number_of_traces, key=lambda l: l[:])
    best_epoch_validation_key_rank = sorted_models[0][2]
    best_key_rankings_validation_key_rank[run] = key_rank_test_epochs[best_epoch_validation_key_rank]
    best_epoch_validation_key_rank_all_runs[run] = best_epoch_validation_key_rank

    # ---------------------------------------------------------------------------------------------------------------- #
    # Process Mutual Information
    # ---------------------------------------------------------------------------------------------------------------- #

    ITY_average_validation += ity_validation

    best_epoch_ITY = np.argmax(ity_validation)
    best_key_rankings_ITY[run] = key_rank_test_epochs[best_epoch_ITY]
    best_epoch_ITY_all_runs[run] = best_epoch_ITY

    print(key_rank_test_epochs[best_epoch_ITY])

    backend.clear_session()

# -------------------------------------------------------------------------------------------------------------------- #
#  Compute Guessing Entropy = Averaged Key Ranking over multiple trainings
# ---------------------------------------------------------------------------------------------------------------------#

guessing_entropy_acc = np.zeros(number_of_traces_test)
guessing_entropy_recall = np.zeros(number_of_traces_test)
guessing_entropy_loss = np.zeros(number_of_traces_test)
guessing_entropy_val_key_rank = np.zeros(number_of_traces_test)
guessing_entropy_ITY = np.zeros(number_of_traces_test)

for run in range(number_of_runs):
    guessing_entropy_acc += best_key_rankings_acc[run]
    guessing_entropy_recall += best_key_rankings_recall[run]
    guessing_entropy_loss += best_key_rankings_loss[run]
    guessing_entropy_val_key_rank += best_key_rankings_validation_key_rank[run]
    guessing_entropy_ITY += best_key_rankings_ITY[run]

guessing_entropy_acc /= number_of_runs
guessing_entropy_recall /= number_of_runs
guessing_entropy_loss /= number_of_runs
guessing_entropy_val_key_rank /= number_of_runs
guessing_entropy_ITY /= number_of_runs

# -------------------------------------------------------------------------------------------------------------------- #
#  Compute Success Rates
# ---------------------------------------------------------------------------------------------------------------------#

success_rate_acc = np.zeros(number_of_traces_test)
success_rate_recall = np.zeros(number_of_traces_test)
success_rate_loss = np.zeros(number_of_traces_test)
success_rate_val_key_rank = np.zeros(number_of_traces_test)
success_rate_ITY = np.zeros(number_of_traces_test)

for index in range(number_of_traces_test):
    for run in range(number_of_runs):
        if best_key_rankings_acc[run][index] == 1:
            success_rate_acc[index] += 1
        if best_key_rankings_recall[run][index] == 1:
            success_rate_recall[index] += 1
        if best_key_rankings_loss[run][index] == 1:
            success_rate_loss[index] += 1
        if best_key_rankings_validation_key_rank[run][index] == 1:
            success_rate_val_key_rank[index] += 1
        if best_key_rankings_ITY[run][index] == 1:
            success_rate_ITY[index] += 1

success_rate_acc /= number_of_runs
success_rate_recall /= number_of_runs
success_rate_loss /= number_of_runs
success_rate_val_key_rank /= number_of_runs
success_rate_ITY /= number_of_runs

# -------------------------------------------------------------------------------------------------------------------- #
#  Compute average ITY
# ---------------------------------------------------------------------------------------------------------------------#

average_ITY = ITY_average_validation / number_of_runs

# -------------------------------------------------------------------------------------------------------------------- #
#  Compute test and validation guessing entropy for each epoch (for trace sets with fixed key)
# ---------------------------------------------------------------------------------------------------------------------#

guessing_entropy_test_per_epoch = np.zeros(param["epochs"])
guessing_entropy_validation_per_epoch = np.zeros(param["epochs"])

for run in range(number_of_runs):
    for epoch in range(param["epochs"]):
        guessing_entropy_test_per_epoch[epoch] += key_rank_test_all_runs[run][epoch][number_of_traces_test - 1]
        guessing_entropy_validation_per_epoch[epoch] += key_rank_val_all_runs[run][epoch][number_of_traces_test - 1]

for epoch in range(param["epochs"]):
    guessing_entropy_test_per_epoch[epoch] /= number_of_runs
    guessing_entropy_validation_per_epoch[epoch] /= number_of_runs

# -------------------------------------------------------------------------------------------------------------------- #
#  Plots
# ---------------------------------------------------------------------------------------------------------------------#

plots = Plots(rows=2, cols=3)
plots.new_plot()

list_of_series = [
    {"data": guessing_entropy_acc, "label": "GE Accuracy", "color": "blue", "legend": True},
    {"data": guessing_entropy_recall, "label": "GE Recall", "color": "orange", "legend": True},
    {"data": guessing_entropy_loss, "label": "GE Loss", "color": "green", "legend": True},
    {"data": guessing_entropy_val_key_rank, "label": "GE Val Key Rank", "color": "red", "legend": True},
    {"data": guessing_entropy_ITY, "label": "GE ITY", "color": "purple", "legend": True}
]
plots.create_line_plot(1, list_of_series, "Number of Traces", "Guessing Entropy", show_legend=True)

list_of_series = [
    {"data": average_ITY, "label": "Average ITY", "color": "blue", "legend": True}
]
plots.create_line_plot(2, list_of_series, "Number of Epochs", "Average ITY", show_legend=True)

list_of_series = [
    {"data": best_epoch_ITY_all_runs, "label": "Best Epoch ITY", "color": "blue", "legend": True},
]
plots.create_distribution_plot(3, list_of_series, "Epoch", "Density", show_legend=True)

list_of_series = [
    {"data": success_rate_acc, "label": "SR Accuracy", "color": "blue", "legend": True},
    {"data": success_rate_recall, "label": "SR Recall", "color": "orange", "legend": True},
    {"data": success_rate_loss, "label": "SR Loss", "color": "green", "legend": True},
    {"data": success_rate_val_key_rank, "label": "SR Val Key Rank", "color": "red", "legend": True},
    {"data": success_rate_ITY, "label": "SR ITY", "color": "purple", "legend": True}
]
plots.create_line_plot(4, list_of_series, "Number of Traces", "Success Rate", show_legend=True)

list_of_series = [
    {"data": guessing_entropy_test_per_epoch, "label": "GE Test", "color": "blue", "legend": True},
    {"data": guessing_entropy_validation_per_epoch, "label": "GE Validation", "color": "orange", "legend": True}
]
plots.create_line_plot(5, list_of_series, "Number of Epochs", "Guessing Entropy", show_legend=True)

list_of_series = [
    {"data": best_epoch_validation_key_rank_all_runs, "label": "Best Epoch Val KR", "color": "blue", "legend": True},
]
plots.create_distribution_plot(6, list_of_series, "Epoch", "Density", show_legend=True)

plots.show_plot()

backend.clear_session()
