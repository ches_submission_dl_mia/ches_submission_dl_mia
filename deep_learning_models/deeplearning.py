from keras import backend as K
from keras.optimizers import SGD, Adam, Adadelta, Adagrad, RMSprop, Adamax, Nadam
from keras.layers import Flatten, Dropout, Dense, Input, Conv1D, MaxPooling1D, BatchNormalization, LSTM, Embedding, \
    AveragePooling1D
from keras import regularizers
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.losses import categorical_crossentropy
from keras.layers import GaussianNoise
from keras.layers.merge import concatenate
import random
import numpy as np
import tensorflow as tf


class DeepLearningModel:

    def __init__(self):
        pass

    def recall_m(self, y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision_m(self, y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    def f1_m(self, y_true, y_pred):
        precision = self.precision_m(y_true, y_pred)
        recall = self.recall_m(y_true, y_pred)
        return 2 * ((precision * recall) / (precision + recall + K.epsilon()))

    def basic_cnn_ches_ctf(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)
        x = Conv1D(filters=16, kernel_size=4, strides=4, activation='relu', padding='valid', name='block1_conv1')(
            trace_input)
        x = Flatten(name='flatten')(x)
        x = Dense(100, activation='relu', name='fc1', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', name='fc2', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(trace_input, x, name='cnn')
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall_m])

        model.summary()

        return model

    def basic_cnn_ascad_random_keys(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)
        x = Conv1D(filters=16, kernel_size=10, strides=10, activation='relu', padding='valid', name='block1_conv1')(
            trace_input)
        x = Flatten(name='flatten')(x)
        x = Dense(100, activation='tanh', name='fc1', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(100, activation='tanh', name='fc2', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(trace_input, x, name='cnn')
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall_m])

        model.summary()

        return model

    def basic_cnn_dpav4(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)
        x = Conv1D(filters=16, kernel_size=10, strides=10, activation='relu', padding='valid', name='block1_conv1')(
            trace_input)
        x = Flatten(name='flatten')(x)
        x = Dense(400, activation='relu', name='fc1', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(400, activation='relu', name='fc2', kernel_initializer='random_uniform',
                  bias_initializer='zeros')(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(trace_input, x, name='cnn')
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall_m])

        model.summary()

        return model

    def basic_mlp_ches_ctf(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        x = Flatten(name='flatten')(trace_input)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        predictions = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(input=trace_input, output=predictions, name='mlp')
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall_m])

        model.summary()

        return model

    def basic_mlp_ixt_ity(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        x = Flatten(name='flatten')(trace_input)
        # x = BatchNormalization()(x)
        x = Dense(200, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(160, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(80, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(40, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(20, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(10, activation='tanh', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        predictions = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(input=trace_input, output=predictions, name='mlp')
        optimizer = Adam(lr=0.00005)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall_m])

        model.summary()

        return model

    def basic_mlp_ches_ctf_dropout(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        x = Flatten(name='flatten')(trace_input)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dropout(0.2)(x)
        x = Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dropout(0.2)(x)
        predictions = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(input=trace_input, output=predictions, name='mlp')
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall_m])

        model.summary()

        return model

    def basic_mlp_ascad_random_keys(self, classes, number_of_samples):
        from keras.models import Model

        input_shape = (number_of_samples, 1)
        trace_input = Input(shape=input_shape)

        x = Flatten(name='flatten')(trace_input)
        x = Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        x = Dense(400, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')(x)
        predictions = Dense(classes, activation='softmax', name='predictions')(x)

        model = Model(input=trace_input, output=predictions, name='mlp')
        optimizer = Adam(lr=0.001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall_m])

        model.summary()

        return model
